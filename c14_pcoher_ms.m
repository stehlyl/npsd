classdef c14_pcoher_ms < handle ; 
    properties
       ac_in = struct() ; 
       amp   = []       ;
       coher = []       ;
       date1 = []       ; 
       date2 = []       ;
       day   = []       ;
       file  = []       ;
       in    = []       ;
       isdata= []       ;
       nzero = []       ;
       nzero_stalta =[] ;
       ref = []         ; 
       time = []        ;
       freq = []        ;
       scale=[]         ;
       eq = struct()    ; 
       sta=struct()     ;
    end
    methods % methods working just for a given scale : 
        function p_coherence_single_scale(h,varargin);
            in.scale=1; 
            in.fs=12;
            in.col='b'; 
            in.xlim=[];
            in = lang.parse_options(in,varargin);
            
            p.date_plot(h.date2(:),h.coher(in.scale,:),'col',in.col,'xlim',in.xlim);
            set(gca,'fontsize',in.fs);
            ylabel('coherency','fontweight','bold','fontsize',14);
            titre= ['coherency at station ',h.in.sta];
            titre= [titre,'  with a sliding window of ',num2str(h.in.norm_nday(in.scale)),' hours'];
            title(titre,'fontsize',18,'fontweight','bold','interpreter','none');
        end
        function p_abs_medmean_single_scale(h,varargin)
            in.scale=1; 
            in.col='b';
            in.nhour=24;
            in.fs=12;
            in.xlim=[];
            in = lang.parse_options(in,varargin);
            
            I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.nhour);
            coher_  = squeeze(h.coher(in.scale,:,:));
            coher_ = coher_(:);
            medmean_ = movmedian(coher_,[I_smooth 0],'omitnan')- movmean(coher_,[I_smooth 0],'omitnan');
            
            p.date_plot(h.date2(:),abs(medmean_),'col',in.col,'xlim',in.xlim);
            set(gca,'fontsize',in.fs);
            ylabel('median-mean','fontweight','bold','fontsize',14);
            titre= [h.in.sta,': median - mean value with a sliding window of ',num2str(in.nhour),'hours'];
            titre= [titre,'  -   coherency computed over ',num2str(h.in.norm_nday(in.scale)),' hours'];
            title(titre,'fontsize',18,'fontweight','bold','interpreter','none');
        end
    end
    methods %plot for all scale in a range of days 
        function p_coherence_all_scale(h,varargin);
            in.scale=1; 
            in.fs=12;
            in.col='b'; 
            in.xlim=[];
            in.caxis=[0.9 1];
            in = lang.parse_options(in,varargin);
            
            p.date_matrix(h.date2(:),h.in.norm_nday,h.coher(:,:),'xlimit',in.xlim,'caxis',in.caxis);
            ylabel('scale [hours]');
            set(gca,'fontsize',in.fs,'fontweight','bold');
            titre= ['coherency at station ',h.in.sta];
            title(titre,'fontsize',18,'fontweight','bold','interpreter','none');
        end
        function p_abs_medmean_all_scale(h,varargin)
            in.scale=1; 
            in.col='b';
            in.nhour=24;
            in.fs=12;
            in.xlim=[];
            in.caxis=[0 0.012];
            in = lang.parse_options(in,varargin);

            I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.nhour);
            nscale = size(h.coher,1);
            date2_  = h.date2(:);
            ndate=numel(date2_);
            medmean=zeros(nscale,ndate);
            for iscale = 1 : nscale
                coher_  = squeeze(h.coher(iscale,:,:));
                coher_ = coher_(:);
                medmean_ = movmedian(coher_,[I_smooth 0],'omitnan')- movmean(coher_,[I_smooth 0],'omitnan');
                medmean(iscale,:)=medmean_(:);
            end
            p.date_matrix(h.date2(:),h.in.norm_nday,abs(medmean),'xlimit',in.xlim,'caxis',in.caxis) ;  
            set(gca,'fontsize',in.fs);
            ylabel('scale [hours]','fontweight','bold','fontsize',14);
            titre= [h.in.sta,': median - mean value with a sliding window of ',num2str(in.nhour),'hours'];
            title(titre,'fontsize',18,'fontweight','bold','interpreter','none');
        end
        
    end 
    methods
        function p_psd_nday(h,date1,date2,varargin)
            in.ch = 1; 
            in.p1 = h.in.p1;
            in.p2 = h.in.p2;
            in.dday=0;
            in.caxis = [0 1]*1e-11 ; 
            in.smooth_nhour=false; 
            in.titre=true;
            in.norm=false;
            in.smoothv=false;
            in.xtick_label=true; 
            in = lang.parse_options(in,varargin);
            
            s=load(h.ac_in.out_file);
            I_day =find(s.day >= date1-in.dday & s.day <= date2+in.dday);
            fft_ = squeeze(s.fft(:,:,I_day));
            fft_ = fft_(:,:);
            if in.smooth_nhour
                I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.smooth_nhour);    
                fft_ = movmean(fft_,[I_smooth 0],2,'omitnan');
            end
            if in.smoothv
                fft_=movmean(fft_,[in.smoothv 0],1,'omitnan');
            end
            if in.norm
                fft_=s2d.norm(fft_);
            end
            date_= s.date2(:,I_day);
            date_= date_(:); 

            p.date_matrix(date_,s.freq,fft_);
            shading('flat');
            set(gca,'Ydir','normal')
            caxis(in.caxis);
            colormap(jet); 
            xlim([date1-in.dday date2+in.dday]);
            ylabel('frequency [hz]','fontweight','bold')
            
            if in.titre
                mn = [num2str((s.date2(2)-s.date2(1))*86400/60),' minutes'];
                titre = [mn,' PSD'];
                if in.smooth_nhour
                    titre=[titre,' averaged over ',num2str(in.smooth_nhour),'h'];
                end
                title(titre,'fontweight','bold');
            end
        end
        function p_psd_single_day(h,cday,varargin) 
            in=struct();
            in.ch = 1; 
            in.p1 = h.in.p1;
            in.p2 = h.in.p2;
            in.dday=0;
            in.caxis = [0 1]*1e-11 ; 
            in.smooth_nhour=false; 
            in.titre=true;
            in.norm=false;
            in.smoothv=false;
            in.xtick_label=true; 
            in = lang.parse_options(in,varargin);

            %extracting psd and averaging it:
            s=load(h.ac_in.out_file);
            I_day =find(s.day >= cday-in.dday & s.day <= cday+in.dday);
            fft_ = squeeze(s.fft(:,:,I_day));
            fft_ = fft_(:,:);
            if in.smooth_nhour
                I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.smooth_nhour);    
                fft_ = movmean(fft_,[I_smooth 0],2,'omitnan');
            end
            if in.smoothv
                fft_=movmean(fft_,[in.smoothv 0],1,'omitnan');
            end
            if in.norm
                fft_=s2d.norm(fft_);
            end
            
            date2= s.date2(:,I_day);
            date2= date2(:); 
            %pcolor 
            pcolor(date2,s.freq,fft_);
            shading('flat');
            caxis(in.caxis);
            colormap(jet); 
            add_colorbar;
            xlim([cday-in.dday cday+1+in.dday]);
            datetick('x','keeplimits')
            add_line_per_day;
            ylabel('frequency [hz]','fontweight','bold')
            %..
            if in.titre
                mn = [num2str((s.date2(2)-s.date2(1))*86400/60),' minutes'];
                titre = [mn,' PSD'];
                if in.smooth_nhour
                    titre=[titre,' averaged over ',num2str(in.smooth_nhour),'h'];
                end
                title(titre,'fontweight','bold');
            end
            if in.xtick_label == false
                set(gca,'xticklabel',{});
            end
        end
        function p_waveform_single_day(h,cday,varargin)
            m=ceil(numel(h.scale)/2);
            in=struct();
            in.ch = 1; 
            in.p1 = h.in.p1;
            in.p2 = h.in.p2;
            in.title= true; 
            in.color=false ; %false; 
            in.color_scale=m;
            in.ylim=false;
            in.dday=0;
            in.caxis = [0.9 1] ;
            in.grid_minor=true;
            in.ylabel=false; 
            in = lang.parse_options(in,varargin);

            for iday = [cday-in.dday:cday+in.dday]
                hold on
                % reading & filtering trace trace
                [h5 success] = lib.pycorr.noise_read_data_single_station(floor(iday), h.ac_in.in_dir);
                if success == false
                    if in.dday==0
                        return
                    else
                        continue
                    end
                end
                h5.trace = s2d.filter(h5.trace,in.p1,in.p2,1/h.ac_in.fe);
                % plotting trace
                if in.color == false
                    plot(h5.date,h5.trace,'--','color',[0.6 0.6 0.6]);
                end
                if in.color
                    I_day = find(h.day == iday); %cday-in.dday & h.day<=cday+1+in.dday); 
                    coher = squeeze(h.coher(m,:,I_day,in.ch));
                    date2 = h.date2(:,I_day);
                    plot_waveform_color(h5,date2,coher');
                end
            end
            % decorating
            xlim([cday-in.dday cday+in.dday+1]);
            datetick('x','keeplimits');
            if in.ylim
                ylim(in.ylim);
            end
            if in.color
                caxis(in.caxis)
                add_colorbar;
            end
            if in.grid_minor==true;
                grid('minor');
            end
            if in.ylabel
                ylabel('waveform','fontweight','bold')
            end
            if in.title
                %titre(1)= {[datestr(cday),' : waveform colored by coherency  -  windows excluded by sta/lta are in grey']};
                %I_day = find(h.day == cday); %cday-in.dday & h.day<=cday+1+in.dday); 
                %aa=num2str(median(h.coher(:,I_day),'omitnan')-mean(h.coher(:,I_day),'omitnan'));
                %bb= num2str(mean(h.coher(:,I_day),'omitnan'));
                %titre(2)={['mean : ',bb,'      mediane - mean : ', aa]};
                %title(titre,'fontweight','bold');
            end
        end
        function [medmean date2_]=p_med_mean_single_day(h,cday,varargin)
            in=struct();
            in.ch = 1; 
            in.caxis=[0 0.013];
            in.title= true; 
            in.nhour=6;
            in.dday=0;
            in = lang.parse_options(in,varargin);
            
            I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.nhour);
            nscale = size(h.coher,1);
            date2_  = h.date2(:);
            I=find(date2_>=cday-in.dday & date2_<cday+1+in.dday);
            ndate=numel(I);
            medmean=zeros(nscale,ndate);
            for iscale = 1 : nscale
                coher_  = squeeze(h.coher(iscale,:,:));
                coher_ = coher_(:);
                medmean_ = movmedian(coher_,[I_smooth 0],'omitnan')- movmean(coher_,[I_smooth 0],'omitnan');
                medmean(iscale,:)=medmean_(I);
            end
            date2_ = date2_(I);
            pcolor(date2_,h.scale,medmean)
            shading('flat');
            ylabel('sliding window','fontweight','bold');
            xlim([cday-in.dday cday+in.dday+1]);
            datetick('x','keeplimits')
            caxis(in.caxis);
            add_colorbar;
            add_line_per_day;
            if in.title
                nhrs = num2str(in.nhour);
                titre= [datestr(cday,'yyyy-mm-dd'),'  median-mean computed with a sliding window of ',nhrs,'h'];
                title(titre,'fontweight','bold')
            end
        end
        function [coher date2]=p_coher_single_day(h,cday,varargin)
            in=struct();
            in.ch = 1; 
            in.caxis=[0.9 1];
            in.dday =0;
            in.title= true; 
            in.smooth_nhour=0;
            in = lang.parse_options(in,varargin);
            
            I_day =find(h.day >= cday-in.dday & h.day <= cday+in.dday);
            coher = squeeze(h.coher(:,:,I_day,in.ch));
            coher = coher(:,:);
            date2 = h.date2(:,I_day); 
            date2 = date2(:);
            
            if in.smooth_nhour >0
                I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.smooth_nhour);
                coher = movmean(coher,[I_smooth 0],2,'omitnan');
            end
            pcolor(date2,h.scale,coher);
            shading('flat')
            
            caxis(in.caxis);
            colormap(jet);
            xlim([cday-in.dday cday+in.dday+1])
            datetick('x','keeplimits');
            ylabel('sliding window','fontweight','bold')
            add_colorbar
            add_line_per_day;
            if in.title
                pers  =[num2str(h.in.p1),'-',num2str(h.in.p2),'s'];
                titre =[datestr(cday,'yyyy-mm-dd'),'  coherency  ',pers];
                title(titre,'fontweight','bold');
            end
        end
    end
    methods
        function I_smooth=get_number_of_time_segment_corresponding_to_N_jour(h,in_nhour)
            nday = floor(in_nhour/24);
            nhour= in_nhour-24*nday  ;
            I_hr=find(24*(h.date2(:,1)-h.date2(1,1))>=nhour,1,'first');
            I_smooth = size(h.coher,1)*nday + I_hr;
        end
        function h=c14_pcoher_ms(in_file)
            s=load(in_file);
            for ifield = fieldnames(s)'
                cfield= ifield{1}      ;
                h.(cfield) = s.(cfield);
            end
            aa=strsplit(in_file,'/');
            aa=aa{end};
            aa=aa(1:end-4);
            h.in.sta=aa;            
        end
    end
end

function add_colorbar()
    hc = colorbar; 
    aa = get(hc,'Position');
    set(hc,'Position',[0.92 aa(2) 0.008, aa(4)])
end

function plot_waveform_color(h5,date2,coher);
    in.factor=1; 
    in.dy=0;
    nseg = size(coher,1);
    date_end=date2(1)+1;
    for iseg = 1: nseg 
        if iseg < nseg 
            I_date = find(h5.date >= date2(iseg) & h5.date <= date2(iseg+1));
        else
            I_date=find(h5.date >= date2(iseg) & h5.date <= date_end) ;%2(1,I_day+1));
        end
        xx = [h5.date(I_date) ; h5.date(I_date)]   ; 
        yy = in.factor*[h5.trace(I_date)'; h5.trace(I_date)']+in.dy; 
        zz = zeros(size(xx))+in.dy; 
        cc = ones(size(xx))*coher(iseg)    ;
        hs=surf(xx,yy,zz,cc,'EdgeColor','interp')  ;
        view(2);
    end
end


function add_line_per_day()
    xlimit = xlim; 
    ylimit = ylim;
    hold on;
    for iday= [floor(xlimit(1)) : ceil(xlimit(2))]
        line([iday iday],ylimit,'color','k','linewidth',4)
    end
end

