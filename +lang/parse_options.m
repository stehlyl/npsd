function in=parse_options(in,inu)
% parse options for both inu=struct and inu=varargin 

  aa=dbstack;
  warning_str=[aa(1).name,' <-',aa(2).name,' <- ', aa(2).file,' '];

  %for backward compatibility : 
  if isstruct(inu)
    inu={inu};
  end
  if isempty(inu);
    inu=in;
  else
    %si inu{1}=structure = user a passer une structure en argument
    if isstruct(inu{1});
      inu=inu{1};
      for ifield=fields(inu)'
        if ~isfield(in,ifield{1})
          dispc([warning_str,': field [',ifield{1},'] do not exist in default input'],'r','b')
        end
        inu = lang.setstructfields(in,inu);
        in = inu;
      end
    elseif iscell(inu)
      %heeeelllppp :-)
      if numel(inu)==1 %
        dispc(in)
      else
        %user a passe une liste argument/valeure : 
        %dispc('im a cell','y')
        for kcell=[1:2:numel(inu)]%inu(1:2:end)
          if isfield(in,inu{kcell})
            in.(inu{kcell})=inu{kcell+1};
          else
            dispc([warning_str,': field [',inu{kcell},'] do not exist in default input'],'r','b')
          end
        end
      end
    end
  end