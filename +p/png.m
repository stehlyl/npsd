function png(format,varargin)
 
% print current figure in an png file whose name is :
% main_1a_plot -> 1a_plot.png
% 
% default dpi is 300 

if ~exist('inu') ; inu=struct();end
in.dpi= 75     ; 
in.tag   = ''  ;
in.format='png';
in.dir='./plot';
in.out_file =[]; 
in.I_dbstack=2;
in=lang.parse_options(in,varargin);

if isempty(in.out_file)
  out_png=lang.get_filename(in.I_dbstack);
  if isempty(in.tag)
    out_png=[out_png(2:end),'.',in.format];
  else
    out_png=[out_png(2:end),'_',in.tag,'.',in.format];
  end
  out_png=[in.dir,'/',out_png];
  os.mkdir(in.dir);
else 
  out_png=in.out_file;
end


switch format
   case{'normal'}
     set(gcf,'PaperPosition',[0.2500    2.5000   8.0000    6.0000])
  case{'large'}
    set(gcf,'PaperPosition',[0.2500    2.5000   33.0000    10.0000])
  case{'large2'}
    set(gcf,'PaperPosition',[0.2500    2.5000   33.0000    15.0000])
  case{'large3'}
    set(gcf,'PaperPosition',[0.2500    2.5000   33.0000    20.0000])
    set(gcf,'PaperOrientation','Portrait')
  case{'large4'}
    set(gcf,'PaperPosition',[0.2500    2.5000   40.0000    15.0000])
    set(gcf,'PaperOrientation','Portrait')
  case{'height'}
    set(gcf,'PaperPosition',[0.2500    2.5000   15.0000    20.0000])
  case{'enlarged'}
    a=get(gcf,'PaperPosition');
    set(gcf,'PaperPosition',[a(1) 1 12 a(4)]);
  case{'big'}
    a=get(gcf,'PaperPosition');
    %set(gcf,'PaperPosition',[-2    0  33.0000    20.0000])
    set(gcf,'PaperPosition',[0    0  33.0000*1.5    20.0000*1.5])
  case{'big2'}
    a=get(gcf,'PaperPosition');
    set(gcf,'PaperPosition',[-2    2.5  33.0000    20.0000])
    %set(gcf,'PaperPosition',[0.25 2.5 16 12]);
end

set(gcf,'InvertHardCopy','off')
dpi=['-r',num2str(in.dpi)];
print(['-d',in.format],out_png,dpi);

switch in.format 
  case{'png'}
    str=['!convert -trim ',out_png,' ',out_png];
    eval(str);
end


