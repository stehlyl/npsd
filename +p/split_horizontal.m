function [h]=split_horizontal(nsubplot,varargin)
in.bottom=0.1; 
in.up=0.9; 
in.x=0.13; 
in.width=0.7750;
in.dy=0                      ; %currently work only for nsubplot=2;
in=lang.parse_options(in,varargin);

sp_bottom=fliplr([in.bottom:(in.up-in.bottom)/(nsubplot):in.up]);

sp_bottom(2)=sp_bottom(2)+in.dy;
sp_height=abs(sp_bottom(2)-sp_bottom(1));

for isubplot=1:nsubplot

  h(isubplot)=subplot('Position',[in.x sp_bottom(isubplot+1) in.width sp_height]);
end

