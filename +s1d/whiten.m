function D=whiten(C,fe,f1,f2,num); 
% whiten the spectrum of signal C, sampled at frequency fe, between frequency f1 and f2,
% apodizing the spectrum over num points. 
%
% INPUT :
% C  : signal to whiten 
% fe : sampling rate in hz of C 
% f1, f2 : frequency band where the whitening is performed. 
% [num=300] : number of points overwhich the spectrum is apodized. 
%
% BECAREFUL IF THE LENGTH OF C SHOULD BE >> THAN NUM (300 by defaults). 

 if ~exist('num') ; num = 300 ; end 
  nwf = numel(C);
  % designing the final spectrum, one in the middle + cos2 over num points at the edge 
  X2=[pi:(1.5*pi-pi)/(num-1):1.5*pi];
  X1=[pi/2:(pi-pi/2)/(num-1):pi];
  apo1=cos(X1).^2;
  apo2=cos(X2).^2;
  npos=(fe/2)*nwf+1; % nombre de points dans la partie pos du spectre...
  nneg=((-fe/nwf)-(-fe/2))/(fe/nwf);
                
  F=0:fe/nwf:fe-fe/nwf;
  [I x1]=find(f1<F,1);
  [I x2]=find(f2<F,1);

  apo(1:round(nwf/2))=1;
  apo(1:x1)=0;
  apo(x1-num+1:x1)=apo1;
  apo(x2:x2+num-1)=apo2;
  apo(x2+num:round(nwf/2))=0;
  apo(round(nwf/2)+1:nwf)=fliplr(apo(1:round(nwf/2)));
  
  % now whitening the spectrum of C
  [A F P]=s2d.fft(C,fe);
  A=A./abs(A);
  A=A.*apo';
  D=real(ifft(A));
  
