function win = get_sliding_window(nwf,tau,win_length,win_shift)
    win_pts   = ceil(win_length/tau) ; 
    win_shift = ceil(win_shift/tau)  ;
    % designe the STA win : 
    I1 = [1 : win_shift : nwf]; 
    I2 = I1 + win_pts         ; 
    I = find(I2 <= nwf)       ;
    I1 = I1(I); 
    I2 = I2(I); 
    win.I1 = I1 ;
    win.I2 = I2 ; 


