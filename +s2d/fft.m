function [A F P]=xfft(C,fe,nfft);
%on def F de 0 a Fe-n/fe en npts. 
%normalement c [0:fe/n:fe/2 -fe/2+fe/n:fe/n:-fe/n]
%entrain si on a 2000 pts il y'a 1001 pts pos et 999 neg
%
%PR utilise freq=0:fs/nsig:fs*(1-1/nsig);

warning('off','MATLAB:divideByZero')
if size(C,1)   ==1 ; C=C'  ; end
[nwf nsig]=size(C);
if exist('nfft')==0 ; nfft=nwf; end


A=fft(C,nfft);
F=fe/2*linspace(0,2,nfft);
P=1./F;
