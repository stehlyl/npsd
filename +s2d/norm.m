function [C]=norm(C,ca,type,c1,c2) 
%  [C]=Norm(C) 
% Normalize each line of C to 1
  
  if exist('ca')~=1  ; ca=0      ; end
  if exist('type')~=1; type='one'; end
  
  if size(C,1) ==1; C=C'; end;
  [a nsig]=size(C);
  
  switch type
   case{'1','one'}
    if ca==0
      for ii=1:nsig
        if min(C(:,ii))==0 & max(C(:,ii))==0
          continue
        end
	C(:,ii)=C(:,ii)/max(abs(C(:,ii)));
      end
    elseif ca==1
      m=round(a/2);
      for ii=1:nsig
	C(1:m-1,ii)=C(1:m-1,ii)/max(abs(C(1:m-1,ii)));
	C(m:a,ii)=C(m:a,ii)/max(abs(C(m:a,ii)));
      end
    end
   case{'AB'}
      C=C-min(min(C)); %0 - X ; 
      C=2*(C/max(max(C))-0.5); %-1-1;
      C=C*(c2-c1)/2;
      C=C+mean([c1 c2]);
  end
    