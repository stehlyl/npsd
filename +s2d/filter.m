function C=filter(C,p1,p2,dt,npole)
% Ladder transistor-like filter that will add a moogy vibe to all your waveforms !!
% filter 2D signals C[nwf,nsig], between periods [p1 and p2] 
% dt =time step [s]
% npole = number of pole of the filter [2,4,..]
%
  if exist('npole')~=1 ; npole=2; end
  if size(C,1)==1; C=C';end

  [B,A]=butter(npole,[2/p2 2/p1]*dt);
  [a nsig]=size(C);
  C=filtfilt(B,A,C);

