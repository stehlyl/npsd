function [str]=num2str(num,tab);
  
  ndigit=numel(num2str(num));
  missing=tab-ndigit;
  str=[];
  if missing > 0 
    for ii=1:missing
      str(end+1)='0';
    end
  end
  str=[str,num2str(num)];
  
  m=numel(str)-tab;
  if m>0
    A= find(str=='.') ;
    if numel(A)>0
      str=str(1:max(numel(str)-m,A));
    end
  end
      
      
