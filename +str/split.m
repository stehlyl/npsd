function dir_names=path_split(str,delimiter);
% INPUT :
%   str = 'dir/dir2/dir3 
% OUTPUT : 
%   dir_names = cell array so that :
%   dir_names{1}='dir'
%   dir_names{2}='dir2'
%   dir_names{3}='dir3'

if exist('delimiter')~=1
  delimiter='/';
end

[A B]=strtok(str,delimiter);
dir_names{1}=A;
while isempty(B)~=1
  [A B]=strtok(B,delimiter);
  dir_names{end+1}=A;
end