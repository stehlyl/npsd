% on a passé le FFT au carré ...
%
% on initialize tout a NaN
% on fait demarrer l'année le 1er decembre de l'année N-1, ainsi on a 396 jours / an
%  ceci permet d'éliminier les effets de bords lors du calcul des cohérences. 
%
% recopier le champs h.in.psd_freq and out2.freq

classdef c11_compute_psd < handle
    properties 
        in = struct ()       ;
        out_dir = ''         ; 
    end
    methods
        function compute_ac_single_year_channel_dset(h,year,ch,dset_name,varargin)
          % compute autcorr for a given year and dataset, loop on all channels
          % USAGE
          %    h = m01_compute_ac(in);
          %    h.compute_ac_single_year_dset(2014,'/IV/NRCA.00');
          %    
          %    typically this function can be called in a loop for different years and dataset
          %    each process will work on a different year / dataset using a simple .lock file procedure. 
          %
          % INPUT : 
          %   in : see the constructor :
          %   year : a single year [2014] 
          %   dset_name : path to noise record within h5 file 
          %
          % OUTPUT 
          %   a .mat file located in _autocorr___Z__stalta__white/IV/NRCA_00/IV_NRCA_00_2014.mat
          in = struct()   ; 
          in.date = false ;
          in = lang.parse_options(in,varargin);	
          dispc(['computing psd for ',dset_name,'-',ch,' ',num2str(year)],'y','b')
          nwf = 86400 * h.in.fe;    
          % getting the name of the input/output file + // 
          h.out_dir = get_outdir_name(h.in)                     ;
          out_file  = get_outfile_name(h.out_dir,year,ch,dset_name);
          h.in.out_file = out_file.out;
          
          if exist(out_file.out,'file'); 
              dispc([  out_file.out, ' : already done'],'r','r');
              return
          end
          if numel(os.dir(out_file.search)) > 0 
              dispc([  out_file.out,' : someone already working on it'],'r','r')
              return
          end
          os.mkdir(out_file.dir)
          create_lock_file(out_file.lock);
          
          % initiazlize the sliding window assuming each daily record goes for midnight-to-midnight.
          md_c = struct(); 
          if in.date == false 
              md_c.date_list = [datenum(year,1,1):1:datenum(year,12,31)];
              %md_c.date_list = [datenum(year-1,12,1): datenum(year-1,12,31) datenum(year,1,1):1:datenum(year,12,31)];
          else
              md_c.date_list = in.date;
          end
              %md_c.date_list = [datenum(year-1,12,1): datenum(year-1,12,31) datenum(year,1,1):1:datenum(year,12,31)];
          md_c.win  = s1d.get_sliding_window(nwf,1/h.in.fe,h.in.win_length,h.in.win_length);
            
          % initialize the output : 
          ndate = numel(md_c.date_list)  ;
          nch = numel(h.in.ch)           ; 
          nfreq = numel(h.in.freq_psd)   ;
          nwin  = numel(md_c.win.I1)     ;
          % loop on date and channels :
          out = struct(); 
          out.dset_name = dset_name             ;
          out.fft  = nan(nfreq,nwin,ndate)      ;
          out.day  = nan(1,ndate)               ;
          out.date1= nan(nwin,ndate)            ; 
          out.date2= nan(nwin,ndate)            ;
          out.isdata=nan(1,ndate)               ;
          out.file = cell(1,ndate)              ;
          out.nzero =nan(nwin,ndate)            ;
          out.nzero_stalta = nan(nwin,ndate)    ;
          if in.lon ~=false ;
              out.sta=struct()         ;
              out.sta.lon = in.lon     ; 
              out.sta.lat = in.lat     ;
              out.sta.dset = dset_name ;
          end
          
          %loop on dte :
          for idate = 1 : ndate
              cday = md_c.date_list(idate)      ;
              msg = ['  ',datestr(now),' : computing psd for ',dset_name,' ',ch,' : ',datestr(cday,'yyyy-mm-dd')];
              dispc(msg,'c','n')
              [ac success] =h.compute_ac_single_day_channel(cday,md_c.win,dset_name,ch);
              % fill the date value independantly even if there wasno data for this day :
              out.day(idate) = cday         ; 
              out.date1(:,idate)=ac.date1   ;
              out.date2(:,idate)=ac.date2   ;
              out.file(idate) = {ac.h5_file}               ;
              if success == false                          ;
                  out.isdata(idate)=0;
                  continue 
              end
              out.isdata(idate)=1                          ;                    
              out.freq  = ac.freq                          ;
              out.fft(:,:,idate)=ac.fft                    ;
              if isfield(ac,'nzero')                       ;  
                  out.nzero(:,idate)=ac.nzero              ; 
                  if isfield(ac,'nzero_stalta')            ;
                      out.nzero_stalta(:,idate) = ac.nzero_stalta; 
                  end
              end
              out.day(idate) = cday         ; 
          end
          out.in   = h.in   ;
          % save the file : 
          dispc(['  saving the result in ',out_file.out],'c','b');
          save(out_file.out,'-struct','out','-v7.3');
          delete_lock_file(out_file.lock);
        end
        function [out success] = compute_ac_single_day_channel(h,cdate,win,dset_name,ch)
          % inner method that compute autocorrelations for a single day / dataset/channel
          %    
          % INPUT : 
          %   cdate : current day in datenumber format (e.g datenum(2016,1,1))
          %   win   : sliding window as returned by s1d.get_sliding_window 
          %   dset_name : name of the dataset to processsed w/o channel i.e /IV/IV_NRCA.00
          %   ch   : channel name 'Z'. 
          %
          % OUTPUT 
          %   success : = true or false depending if we could read & process the data 
          %   out     : structure containing the result. 
          out = struct(); 
          success = true ; 
          
          % determining the name of the h5 file to read
          datev = datevec(cdate)    ; 
          year  = num2str(datev(1)) ;
          day   = str.num2str(datenum(cdate) - datenum(datev(1),0,0),3);
          out.h5_file = [h.in.in_dir,'/',year,'/day_',day,'.h5'];
          
          %compute the date vector corresponding to the beg/end of each AC :
          out.date1 = cdate + win.I1 / (86400 * h.in.fe) ;
          out.date2 = cdate + win.I2 / (86400 * h.in.fe) ;
          % trying to read the data 
          try 
              cdset = [dset_name,'/',ch];
              data = double(h5read(out.h5_file,cdset)); 
          catch
              success = false ;
              dispc(['    could not read ',out.h5_file, ' ',cdset],'r','r')
              return
          end
          
          
          % now pre-process this daily noise record 
          datap=process_this_day_noise_record(h,data)    ;
          % compute the autocorr : 
          nwin = numel(win.I1)                           ; 
          
          %% Compute psd : 
          nfreq = numel(h.in.freq_psd); 
          out.fft = nan(nfreq,nwin)   ;
          out.freq= h.in.freq_psd     ; 

          switch h.in.method 
            case 'fft'
          	  nfft = win.I2(1)-win.I1(1)+1 ;
          	  data_fft = zeros(nfft,nwin)  ;
              for iwin = 1 : nwin 
                  data_fft(:,iwin) = datap.data(win.I1(iwin):win.I2(iwin));
              end
	  		  freq=h.in.fe/2*linspace(0,2,nfft);
              toto=fft(data_fft);
              out.fft=interp1(freq,abs(toto).^2,h.in.freq_psd,'linear');
            case 'psd'
              for iwin= 1 : nwin 
                  cwf = datap.data(win.I1(iwin):win.I2(iwin));
                  %compute psd and smooth it
                  Hs=spectrum.periodogram;
                  xx=psd(Hs,cwf,'Fs',h.in.fe);
                  for ifreq=1:nfreq;
                      if ifreq < nfreq 
                      	df=(h.in.freq_psd(ifreq+1)-h.in.freq_psd(ifreq))/2;
					  else 
						df=(h.in.freq_psd(ifreq) - h.in.freq_psd(ifreq-1))/2;
					  end	
                      I1=find(xx.Frequencies >= h.in.freq_psd(ifreq)-df,1,'first');
                      I2=find(xx.Frequencies <= h.in.freq_psd(ifreq)+df,1,'last');
                      out.fft(ifreq,iwin) =mean(xx.Data(I1:I2));
                  end
              end
          end
          %% optionnaly measure the number of zero within the  dataset :
          if h.in.keep_zero 
              out.nzero = zeros(1,nwin);
              for iwin = 1 :nwin
                  J1 = win.I1(iwin); 
                  J2 = win.I2(iwin); 
                  [out.nzero(:,iwin)] = numel(find(datap.datao(J1:J2)==0));
              end
              if h.in.stalta 
                  out.nzero_stalta = zeros(1,nwin);
                  for iwin = 1 :nwin
                      J1 = win.I1(iwin); 
                      J2 = win.I2(iwin); 
                      [out.nzero_stalta(:,iwin)] = numel(find(datap.data_stalta(J1:J2)==0));
                  end
              end
          end
      end
      function out=process_this_day_noise_record(h,data)
        % now pre-process this daily noise record 
        out.datao = data                    ; %original dataset 
        out.data  = data                    ;
        if  h.in.stalta
            out.data = sta_lta(out.data,h.in.fe);
            out.data_stalta = out.data          ; % original with stalta
        end
        if h.in.white
            out.data = s1d.whiten(out.data,h.in.fe,0.05,2,300);
        end
        if h.in.filter
            out.data = s2d.filter(out.data,h.in.filter_p1, h.in.filter_p2,1./h.in.fe);
        end
        if h.in.normenv
           out.data = out.data./abs(hilbert(out.data));
        end
        out.time = linspace(0,86400,numel(data));
      end
    end
    methods
        function h=c11_compute_psd(varargin)
            in.in_dir='./data_5.0hz/daily/IV';
            in.ch = 'Z'          ;
            in.fe  = 5           ; 
            in.win_length = 300  ;  
            in.keep_zero=true    ;  % garder en memoire les zeros
            in.stalta= true      ;  % application du sta/lta 
            in.white = true      ;
            in.filter = false    ; 
            in.filter_p1 = 1/0.7 ;
            in.filter_p2 = 1/0.5 ;
            in.normenv  = false  ; 
            in.method = 'fft'    ;
            h.in = lang.parse_options(in,varargin);
        end
    end
end


%============ FUNCTIONS THAT ARE OUTSIDE THE CLASS  ============================

function out_file  = get_outfile_name(out_dir,year,ch,dset_name)
    % get ouput file name for a given year / dataset
    % out_dir : _autocorr___Z__stalta__white
    % year : 2014 
    % dset_name : /IV/NRCA.00
    pid = num2str(feature('getpid'))                ;
    dset_name = strrep(dset_name,'.','_')           ;
    out_file=struct()                               ; 
    out_file.dir   = [out_dir,dset_name]        ;
    dset_name = strrep(dset_name(2:end),'/','_')    ;
    out_file.root  = [out_file.dir,'/',dset_name,'_',num2str(year),'_',ch];
    out_file.out   = [out_file.root,'.mat']         ;
    out_file.lock  = [out_file.root,'.',pid,'.lock'];
    out_file.search= [out_file.root,'.*']           ;
end


function out_dir = get_outdir_name(in)
  % get main output directory name from user input :
  % out_dir : _autocorr___Z__stalta__white
  aa = dbstack(3);
  prefix = str.split(aa.name,'_');
  prefix = prefix{1};

  switch in.method 
	case 'fft' 
 	 	out_dir =['+c11_fft2',prefix]    ;
	case 'psd'
  		out_dir=['+c11_psd',prefix] ;
  end		
  out_dir =[out_dir,'_',str.num2str(in.win_length,4),'s'];
  freq=[num2str(in.freq_psd(1)),'hz_',num2str(in.freq_psd(end)),'hz_',num2str(in.freq_psd(2)-in.freq_psd(1)),'hz'];
  out_dir = [out_dir,'_',strrep(freq,'.','x')];
	
  if in.stalta
      out_dir = [out_dir,'__stalta'];
  end
  if in.white
      out_dir= [out_dir,'__white']  ; 
  end
  if in.filter 
      out_dir =[out_dir,'__filter_']; 
      out_dir =[out_dir,num2str(in.filter_p1,3),'_',num2str(in.filter_p2,3)];
  end
  if in.normenv
      out_dir =[out_dir,'__normenv'];
  end
  out_dir = strrep(out_dir,'.','x') ;
end



%-------------------------------------
function delete_lock_file(filename)
    if exist(filename,'file');
        delete(filename);
    end
end


function create_lock_file(filename)
    aa=3; 
    save(filename,'aa')
end





%========= PROCESSING FUNCTIONS :================
function data=sta_lta(data,Fs)
    npoles =2 ; 
    npass = 2 ;
    %data  = data - nan.mean(data);
    % Detrend
    %data = detrend(data);
    Wn_list = [[0.03 1];[1 2]];
    window_list = [60;3];
    max_threshold_list = [1.5;2];
    min_threshold_list = [0;0];

    for elts = [Wn_list window_list max_threshold_list min_threshold_list]'
        % filtering
        current_data = butterworth_filter(data,elts(1),elts(2),Fs,npoles,npass);
        % time window
        window_time =  elts(3)    ;  %in seconds
        window = window_time * Fs ; % number of points
        % STA/LTA processing and data removal
        [res, del, sta_list, lta] = sta_lta_processing(current_data,window,elts(4),elts(5));
        data(del) = 0;
    end
end





function s_filt = butterworth_filter(signal,f1,f2,fs,npoles,npass)
  % Normalized cutoff frequencies
  Wn = 2.*[f1,f2] / fs;
      
  % Compute coefficient of the filter
  [B,A] = butter(npoles,Wn);
  
  % Application of the filter
  if npass == 1
      s_filt = filter(B,A,signal);
      elseif npass == 2
          s_filt = filtfilt(B,A,signal);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sta_lta_processing
% Description: moving window STA/LTA-based signal processing. Put to 0 a
% window where STA/LTA is belwow a threshold. 
% Inputs: 
% - a vector (the signal we want to process)
% - an integer (representing a window length)
% - an integer (representing the desired maximum threshold)
% - an integer (representing the desired minimum threshold)
% Output: 
% - a vector (the processed signal)
% - a list of couple (representing each domain deleted of the signal)
% - a list of integer (representing lta for each window)
% - an integer (representing the lta of the signal)

function [s del_area sta_list lta] = sta_lta_processing(signal, window, max_threshold, min_threshold)
  % Initialization
  s = zeros(size(signal));
  del_area = [];
  sta_list = [];

  % Long time average of the entire signal (except values near zero)
  lta = rms(signal(find(or(signal > 10^-12,signal < -10^-12))));

  % Processing the signal for each window
  nb_window = ceil(length(signal)/window);

  for i_win = 0:nb_window - 1
      % Min and max limits for the window
      min = (i_win * window) + 1 - 2*window;
      if min < 1
          min = 1;
      end
      max = ((i_win + 1) * window) + 2*window;
      if max > length(signal)
          max = length(signal);
      end
      % Short time average
      sta = rms(signal(min:max));
      sta_list = [sta_list sta];
      
      % Ratio STA/LTA for each window
      ratio = sta / lta;
      % If the ratio is strictly above the threshold, the signal is set to 0
      if (ratio > max_threshold) || (ratio < min_threshold)
          s(min:max) = 0;
          del_area = [del_area min:max];
      else
          s(min:max) = signal(min:max);
      end
  end
end
