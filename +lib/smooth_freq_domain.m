function [psd_smooth win_fft] = smooth_freq_domain(psd_,type,freq,f1,f2,fs,win_length)
    win_fft   = get_smoothing_window(type,freq,fs,win_length);
    psd_smooth = apply_smoothing(psd_,win_fft,freq,f1,f2)     ;
    %ref_smooth = apply_smoothing(ref,win_fft,freq,f1,f2)      ; 
end

function psd_smooth= apply_smoothing(psd_,win_fft,freq,f1,f2) 
    nwf     = numel(freq) ;
    npsd    = size(psd_,2);
    psd_smooth=zeros(size(psd_));
    I1 = find(freq>f1,1,'first');
    I2 = find(freq<f2,1,'last') ; 

    if npsd == 1 
        psd_(1:I1-1)  =0        ;
        psd_(I2+1:end)=0        ;
        toto=conv(psd_, win_fft); 
        toto=toto(1:nwf)        ;
        psd_smooth=toto         ;
    else
        for ipsd = 1 :npsd 
            psdc =psd_(:,ipsd)      ; % 1st rm frequency outside the band :
            psdc(1:I1-1)=0          ;
            psdc(I2+1:end)=0        ;
            toto=conv(psdc, win_fft); 
            toto=toto(1:nwf)        ;
            psd_smooth(:,ipsd)=toto ;
        end
    end
end



function win  = get_smoothing_window(type,freq,fs,win_length)
    smooth_ = struct()   ;
    nwf     = numel(freq); 
    switch type
        case{'hgate','half gate','half_gate'}
          t2      = nwf/fs     ;
          smooth_.time = [0 : 1./fs:t2*2];
          smooth_.time = smooth_.time(1:nwf*2);
          I1=find(smooth_.time >=win_length,1,'first');
          smooth_.win  = zeros(1,numel(smooth_.time)); 
          smooth_.win(1:I1)=1;
          [smooth_.win_fft smooth_.F smooth_.P] = s2d.fft(smooth_.win,fs);
          smooth_.F = smooth_.F(1:nwf);
          smooth_.win_fft = smooth_.win_fft(1:nwf);
          smooth_.win_ffti = interp1(smooth_.F,smooth_.win_fft,freq,'spline');
          win = abs(smooth_.win_ffti);
      case{'hanning'}
        df = freq(2)-freq(1)                      ;
        win_length_fft = ceil(1.5/(df*win_length));
        han = hanning(win_length_fft)             ;
        nhan = floor(numel(han)/2)                ;
        han= han(nhan:end);
        win = zeros(1,nwf);
        ncopy= min(nwf,numel(han));
        win(1:ncopy) = win(1:ncopy)+han(1:ncopy)';
    end
        
end