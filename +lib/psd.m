function [D]=psd(A,time,win_length,win_shift,freqi)
  in.win_length=win_length; 
  in.win_shift =win_shift ; 
  in.freqi     =freqi ;
  

  tau=time(2)-time(1);
  nfreq=numel(in.freqi);
  nwf  =numel(A);
  
  %% first window the signal : 
  width=floor(in.win_length/tau); %pts 
  shift=ceil(in.win_shift/tau)  ;
  win1=[1:shift:numel(A)-width] ; %pts
  win2=win1+width               ;
  nwin=numel(win1)              ;
  %% initialize output 
  D.fft=zeros(nfreq,nwin)    ;
  D.fftc=zeros(nfreq,nwin)   ;
  D.psd=zeros(nfreq,nwin)    ;
  D.f=in.freqi               ; 
  D.t1=zeros(1,numel(win1))  ;
  D.t2=zeros(1,numel(win1))  ;
  
  %% apply taper to A
  ntaper=round(5*win_length/100)    ;
  taper_t1=ntaper+time(1)           ;
  taper_t2=time(end)-ntaper  ;
  taper = taper_cos2(time,taper_t1,taper_t2,ntaper);
  A=A.*taper';
  
  
  Hs=spectrum.periodogram;
  %get frequency vector of the psd : 
  C=A(win1(1):win2(1));
  xx=psd(Hs,C,'Fs',1/tau);
  %average psd between each frequency 
  I1=zeros(1,nfreq);
  I2=I1;
  for ifreq=1:nfreq-1 ; 
    df=(in.freqi(ifreq+1)-in.freqi(ifreq))/2;
    I1(ifreq)=find(xx.Frequencies >= in.freqi(ifreq)-df,1,'first');
    I2(ifreq)=find(xx.Frequencies <= in.freqi(ifreq)+df,1,'last');
  end
  
  %%main loop 
  for iwin=1:numel(win1)
    C=A(win1(iwin):win2(iwin));
    D.t1(iwin)=time(win1(iwin));
    D.t2(iwin)=time(win2(iwin));
    
    %compute fft
    [B freq]=s2d.fft(C,1/tau);
    D.fft(:,iwin)=interp1(freq,abs(B),in.freqi,'spline');
    D.fftc(:,iwin)=interp1(freq,B,in.freqi,'spline');
    %compute psd and smooth it
    xx=psd(Hs,C,'Fs',1/tau);
    for ifreq=1:nfreq-1;
      D.psd(ifreq,iwin)=mean(xx.Data(I1(ifreq):I2(ifreq)));
    end
  end
end
  
  
function taper=taper_cos2(time,t1,t2,taper_length);
%time = time vector 
%t1   = time of the 1st point that not tapered 
%t2   = time of the last point not tapered  
%taper_length=nombre de point de la transition 

tau=time(2)-time(1);
fe=1/tau;
nwf=numel(time);

X2=[pi:(1.5*pi-pi)/(taper_length-1):1.5*pi]; %seconde
X1=[pi/2:(pi-pi/2)/(taper_length-1):pi]    ;
apo1=cos(X1).^2;
apo2=cos(X2).^2;

taper=zeros(numel(time),1);
It1=find(time>=t1,1,'first');
It2=find(time>=t2,1,'first');
taper(It1:It2)=1;
if ~isempty(It1) 
  taper_length_left=min(t1,taper_length)        ; % seconde
  taper_start =t1-taper_length_left;            ; % seconde
  Itaper_start=find(time>=taper_start,1,'first'); % pts
  Itaper_length_left=taper_length_left/tau      ; % pts
  X1=[pi/2:(pi-pi/2)/(Itaper_length_left-1):pi] ;
  apo1=cos(X1).^2                               ;
  if isnan(apo1)==0
    taper(Itaper_start:Itaper_start+numel(apo1)-1)=apo1;
  end
end
if ~isempty(It2)
  taper_length_right=min(time(end)-t2,taper_length)        ; % seconde
  taper_start =t2 ;
  Itaper_start=It2 ; %find(time>=taper_start,1,'first'); % pts
  Itaper_length_right=taper_length_right/tau      ; % pts
  X2=[pi:(1.5*pi-pi)/(Itaper_length_right-1):1.5*pi];
  %  X2=[pi/2:(pi-pi/2)/(Itaper_length_right-1):pi] ;
  apo2=cos(X2).^2                               ;
  taper(Itaper_start:Itaper_start+numel(apo2)-1)=apo2;
end
end
  