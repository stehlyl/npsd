function [h5,success]=noise_read_data_single_station(cday,data_dir)
    success= true;
    cday_vec = datevec(cday); 
    cyear = cday_vec(1); 
    cjul  = cday - datenum(cyear,1,1)+1;

    cfile = [data_dir,'/',num2str(cyear),'/day_',str.num2str(cjul,3),'.h5'];
    
    try 
        h5 = struct();
        h5.trace = h5read(cfile,'/IV/NRCA.00/Z');
        h5.trace = double(h5.trace);
    catch
        dispc(['no data found for NRCA in ',cfile],'c','b')
        h5 = struct();
        success=false ;
        return
    end
    
    h5.date = linspace(cday,cday+1,numel(h5.trace));
    h5.t    = [h5.date-h5.date(1)]*86400;
    %h5.tracef = s2d.filter(h5.trace,p1,p2,h5.t(2)-h5.t(1));
end
