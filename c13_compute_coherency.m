classdef c13_compute_coherency < handle 
    properties
        in = struct() ; 
    end
    methods
        function compute_coher_single_station(h,station_path)
            out_name = h.get_outname(station_path) ;
            os.mkdir(out_name.dir) ;
            os.mkdir(out_name.dir2);
            % has this file already been processed 
            if exist(out_name.file,'file')
                dispc(['  ',out_name.file,'  already done'],'r','r')
                return
            end
            % is already being done : 
            if numel(os.dir(out_name.search)) > 0 
                dispc(['  ',out_name.file,':  someone is already working on it'],'r','r')
                return
            end
            % no lets go 
            create_lock_file(out_name.lock);
            
            is_data = 0; % count number of year ch for which we have data
            for ich = 1 : numel(h.in.ch)
                % computing coherency year per year : 1 file per year :
                kyear=0;
                for iyear = [h.in.year1 : h.in.year2]
                    kyear = kyear+1;
                    % see if we have an autocorr file for this year / channel :
                    cfile = [h.in.in_dir,'/',station_path,'/',strrep(station_path,'/','_'),'_',num2str(iyear)];
                    cfile = [cfile,'_',h.in.ch(ich),'.mat'];
                    if ~exist(cfile,'file') 
                        msg=['  ',cfile,' not found => cannot compute coherency'];
                        dispc(msg,'r','r'); 
                        continue 
                    end
                    % load the file 
                    dispc(['  ',datestr(now),' : loading ',cfile],'c','b');
                    s_ori=load(cfile);
                    if sum(s_ori.isdata)==0
                        dispc(['  ',datestr(now),' : no data for ',cfile],'c','b');
                        continue
                    end
                    dispc(['  ',datestr(now),' : computing coherency'],'c','b');
                    is_data = is_data+1;
                    % measure the energy/amplitude for that day 
                    amp = h.measure_amp(s_ori);
                    % compute the coherency for this year/ch :
                    out(kyear,ich) = h.compute_coherency_freq_domain_single_year(s_ori,iyear);
                    
                    if isfield(s_ori,'sta');
                        out(kyear,ich).sta=s_ori.sta;
                    end
                    out(kyear,ich).amp = amp;
                    clear s_ori;
                end
            end
            % concatenate the result and save if we had some data:
            if is_data~=0 
                out = h.flattened_out(out);
                save(out_name.file,'-struct','out','-v7.3')
                delete_lock_file(out_name.lock)
            end
        end
        
        function out = compute_coherency_freq_domain_single_year(h,s_ori,cyear);
            out=struct() ;
            df = s_ori.freq(2)-s_ori.freq(1);
            % find psd of the current year 
            Idate = find(s_ori.day>= datenum(cyear,1,1));
            ndate = numel(Idate)                        ;  
            % initializing outputs :
            nmeas = size(s_ori.date1,1)                 ;
            nscale = numel(h.in.norm_nday)              ;
            out.coher = nan(nscale,nmeas,ndate)         ;
            
            if h.in.pre_smooth_psd 
                s_ori = h.pre_smooth_psd(s_ori);
            end
            
            for iscale = 1 :nscale
                s=h.normalize_psd(s_ori,iscale) ;

                F = s.in.freq_psd ; 
                I_F = find(F>= 1./h.in.p2 & F<= 1./h.in.p1);
                ref_smooth = ones(size(F));
                if h.in.smooth_psd_as_ac 
                    f1 = 1./h.in.p2; 
                    f2 = 1./h.in.p1;
                    t_max = h.in.smooth_psd_as_ac_t ;
                    [ref_smooth] = lib.smooth_freq_domain(abs(ref_smooth'),'hanning',F,f1,f2,1./s.in.fe,t_max);   
                end
                Y = ref_smooth(I_F);
                ene_ref=sum(Y.^2)  ;
                for idate = 1 : ndate;
                    cdate = Idate(idate) ;
                    psd_smooth = squeeze(s.fft(:,:,cdate));
                    if h.in.smooth_psd_as_ac
                        [psd_smooth] = lib.smooth_freq_domain(abs(psd_smooth),'hanning',F,f1,f2,1./s.in.fe,t_max);   
                    end
                    X = psd_smooth(I_F,:) ;
                    C=sum(X.*Y')          ;
                    ene_ac =sum(X.^2)     ;
                    out.coher(iscale,:,idate) = C./sqrt(ene_ac.*ene_ref) ;
                end
                if iscale==1
                    out.date1 = s.date1(:,Idate)     ; 
                    out.date2 = s.date2(:,Idate)     ; 
                    out.day   = s.day(Idate)         ; 
                    out.ac_in = s.in                 ; 
                    out.isdata= s.isdata(Idate)      ;
                    out.scale = h.in.norm_nday       ;
                    if isfield(s,'nzero')            ;
                        out.nzero = s.nzero(:,Idate) ;
                    end
                    if isfield(s,'nzero_stalta')       ;
                        out.nzero_stalta=s.nzero_stalta(:,Idate); 
                    end
                    out.freq = s.in.freq_psd;
                    out.file = s.file(Idate) ;
                end
            end
        end
        function out2 = flattened_out(h,out)
        % compute_coherency return an array of structure out[nyear,nch] 
        % here we flattens the result so that that we have a single structure out2 containing the results 
        %    for all year/ch
        % The downside of this approcach is that we duplicate temporarily the results which are few Mb. 
            [nyear nch]  = size(out)           ;
            nscale = size(out.coher,1)         ;
            nfreq  = numel(out(1,1).freq)      ;
            out2 = struct()                    ;
            out2.date1 = [out(1:nyear,1).date1];
            out2.date2 = [out(1:nyear,1).date2];
            out2.day   = [out(1:nyear,1).day  ];
            out2.ac_in = out(1,1).ac_in        ; 
            out2.freq   = out.freq             ;
            out2.scale = out(1,1).scale        ; 
            out2.in    = h.in                  ;
            out2.file  = [out(:,1).file]       ;
            [nmeas nday] = size(out2.date1)    ;
            out2.amp  = nan(nmeas,nday,nch)  ; 
            out2.coher= zeros(nscale,nmeas,nday,nch)   ;
            %out2.coher= zeros(nmeas,nday,nscale,nch)  ;
            out2.isdata=zeros(nch,nday)        ; 
            out2.nzero =zeros(nmeas,nday,nch)  ; 
            out2.nzero_stalta = zeros(nmeas,nday,nch)  ;
            for ich =1 : nch 
                out2.amp(:,:,ich)  =[out(:,ich).amp]   ;
                out2.coher(:,:,:,ich)=[out(:,ich).coher] ;
                out2.isdata(ich,:) =[out(:,ich).isdata];
                out2.nzero(:,:,ich)=[out(:,ich).nzero] ; 
                out2.nzero_stalta(:,:,ich)=[out(:,ich).nzero_stalta];
            end
            if isfield(out,'sta');
                out2.sta=out.sta;
            end
        end
    end

    methods % methods that apply on s (fft)
        function s = normalize_psd(h,s,iscale)
            msg=['  normalizing psd using ',h.in.norm_type,' and ',num2str(h.in.norm_nday(iscale)),' days'];
            dispc(msg,'c','n')
            [nfreq npsd_per_day nday] = size(s.fft);
            F = s.in.freq_psd ; 
            I_F = find(F>= 1./h.in.p2 & F<= 1./h.in.p1);
            switch h.in.norm_type
              case{'mean'}
                fh = @movmean ;
              case{'med','median'}
                fh = @movmedian;
            end
            fft_ = s.fft(:,:); 
            if h.in.norm_psd_by_amp
                fft_ = s2d.norm(fft_);
            end
            if h.in.norm_psd_by_amp
                fft_amp = sum(fft_(I_F,:)); 
                fft_ = fft_ ./ fft_amp ;
            end
            I_smooth = ceil(npsd_per_day * h.in.norm_nday(iscale));
            s.fft= reshape(fft_./fh(fft_,[I_smooth 0],2,'omitnan'),size(s.fft));
            
        end
        
        function s_ori=pre_smooth_psd(h,s_ori);
            nday = floor(h.in.pre_smooth_nhour/24);
            nhour= h.in.pre_smooth_nhour-24*nday  ;
            I_hr=find(24*(s_ori.date2(:,1)-s_ori.date2(1,1))>=nhour,1,'first');
            I_smoothh = size(s_ori.fft,2)*nday + I_hr;

            df = s_ori.freq(2)-s_ori.freq(1);
            I_smoothv=ceil(h.in.pre_smooth_psd_smoothv/df);
            
            fft_ = s_ori.fft(:,:);
            fft_ = movmean(fft_,[I_smoothh 0],2,'omitnan');
            fft_ = movmean(fft_,[I_smoothv 0],1,'omitnan');
            s_ori.fft = reshape(fft_,size(s_ori.fft));
            
        end
        function amp = measure_amp(h,s)
            I_freq = find(s.freq <= 1./h.in.p1 & s.freq >= 1./h.in.p2);
            [nfreq nseg nday] = size(s.fft);
            amp=squeeze(sum(s.fft(I_freq,:,:),'omitnan'));
        end
    end
    
    methods % simple methods :
        function out = get_outname(h,station_path) 
            aa=strsplit(station_path,'/')   ;
            pid = num2str(feature('getpid'));
            out = struct()                  ;
            out.dir =['+c13_coher']   ;
            if ~isempty(h.in.tag)           ;
                out.dir = [out.dir,'_',h.in.tag];
            end
            out.dir =[out.dir,'/',h.in.in_dir,'__',h.in.ch,'__',num2str(h.in.year1),'_',num2str(h.in.year2)];
            out.dir =[out.dir,'__',str.num2str(h.in.p1,3),'_',str.num2str(h.in.p2,3),'s'];

            if h.in.pre_smooth_psd 
                out.dir =[out.dir,'__presmoothed'];
            end
            if h.in.smooth_psd_as_ac
                out.dir =[out.dir,'__smoothed_as_ac_',num2str(h.in.smooth_psd_as_ac_t),'s'];
            end
            if h.in.norm_psd_by_max
                out.dir=[out.dir,'__psd_normed_by_max'];
            end
            if h.in.norm_psd_by_amp 
                out.dir=[out.dir,'__psd_normed_by_amp'];
            end
            out.dir =[out.dir,'__',h.in.norm_type,'_',num2str(numel(h.in.norm_nday)),'_scales'];

            out.dir = strrep(out.dir,'.','x')   ;
            out.dir2= [out.dir,'/',aa{1}]       ;
            out.root= [out.dir,'/',station_path];
            out.file= [out.root,'.mat']         ;
            out.lock= [out.root,'.',pid,'.lock'];
            out.search=[out.root,'.*']          ;
        end

    end
    
    methods % constructor :
        function h=c13_compute_coherency(varargin) ;
            in = struct()   ;
            in.in_dir ='+c11_psd__0300s_0x4hz_1hz_0x005hz__stalta';
            in.year1 = 2016 ; 
            in.year2 = 2016 ; 
            in.ch = 'Z'     ;
            
            in.pre_smooth_psd = true      ; %pre-smooth the matrix of PSD ?
            in.pre_smooth_psd_smoothv=0.05; %hz : vertical smoothing length over the vertical (freq) axix
            in.pre_smooth_nhour = 0.5         ; %   : horizontal smoothing over the horizontal (time) axis

            in.filter = true;
            in.p1  = 1/0.7  ;
            in.p2  = 1/0.5  ;

            in.norm_psd_by_max = true     ; % when normalizing current PSD by the N-hour previous PSD
            in.norm_psd_by_amp = true     ; % should we normalize the N-hour PSD by their max or amp ?
            
            in.smooth_psd_as_ac=false     ; % when computing coherency : smooth the REF and current PSD ?
            in.smooth_psd_as_ac_t =   5   ; % this emulate time-windowing of current, and ref AC    
            
            in.norm_nday=[1 2 4 8 16 24 48 72]  ; 
            in.norm_type='mean';
            in.tag='';
            h.in = lang.parse_options(in,varargin);
        end
    end
end







%------- FUNCTION OUTSIDE THE CLASS : -------------------
function delete_lock_file(filename)
    if exist(filename,'file');
        delete(filename);
    end
end

function create_lock_file(filename)
    aa=3; 
    save(filename,'aa')
end

