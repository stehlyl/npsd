function mkdir(str1)
% INPUT :
%  str1 = string in the form of 'aaa/bbb/ccc' 
% OUTPUT 
% create dir aaa, aaa/bbb, aaa/bbb/ccc if they dont exist.

dir_names=str.split(str1);
cdir='';
for idir=dir_names;
  if isempty(cdir);
    cdir=idir{1};
  else
    cdir=[cdir,'/',idir{1}];
  end
  if ~exist(cdir,'dir')
    mkdir(cdir);
  end
end