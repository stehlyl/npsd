function files=dir(pattern)


[a raw]=system(['ls -1d ', pattern]);

files={};
[A B]=strtok(raw,sprintf('\n'));
while numel(B) >0
  files{end+1}=A(1:end);
  [A B]=strtok(B,sprintf('\n'));
end

if numel(files)==1
  if numel(strfind(files{1},'No such file or directory')) >0
    files={};
  elseif numel(strfind(files{1},'Aucun fichier ou dossier de ce type'))>0
    files={};
  elseif numel(strfind(files{1},'no matches found'))>0
    files={};
  elseif numel(strfind(files{1},'zsh:1:'))>0
    files={};
  end
end