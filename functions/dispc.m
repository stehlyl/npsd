function dispc(text,color,varargin)
% display text into color 'rgybmcw' with font type ['bold','underline',...]
% possible colors are : 'r','g','y','b','m','c','w','grey'
% possible font_type (varargin) are : '[b]old','[u]nderline','[d]ark','[r]everse'
if exist('color')~=1 ; color='white'; end


%% determining color
if ~isnumeric(color)
  switch color
    case {'red','r'}
      ncolor=31;
    case{'green','g'}
      ncolor=32;
    case{'yellow','y'}
      ncolor=33;
    case{'blue','b'}
      ncolor=34;
    case{'magenta','m'}
      ncolor=35;
    case{'cyan','c'}
      ncolor=36;
    case {'white','w'}
      ncolor=37;
    case{'gray','grey'}
      ncolor=38;
  end
else
  ncolor=color;
end
%% determining font_type 
prefix={};
if nargin > 2
  for ifont=varargin
    switch ifont{1}
      case{'bold','b'}
        prefix{end+1}='\x1b[1m';
      case{'dark','d'}
        prefix{end+1}='\x1b[2m';
      case{'underline','under','u'}
        prefix{end+1}='\x1b[4m';
      case{'blink'}
        prefix{end+1}='\x1b[5m';
      case{'reverse','r'}
        prefix{end+1}='\x1b[7m';
    end
  end
end

%% merging text+color+font_type
if isstr(text)
  str_=[];
  for iprefix=1:numel(prefix)
    str_=[str_,prefix{iprefix}];
  end
  str_=[str_,'\x1b[',num2str(ncolor),'m',text,'\x1b[0m\n'];
  fprintf(str_);
elseif isstruct(text)
  ntab=0;
  for ifield=fieldnames(text)'
    ntab=max(numel(ifield{1}),ntab);
  end
  ntab=min(10,ntab);
  for ifield=sort(fieldnames(text)');
    clear str_
    str_=[str.tab(ifield{1},ntab),' : '];
    a=getfield(text,ifield{1});
    if isstr(a) || ischar(a)
      dispc_no_line_end(str_,'r','b');
      dispc(a, 'r');
    end
  end
  for ifield=sort(fieldnames(text)')
    str_=[str.tab(ifield{1},ntab),' : '];
    a=getfield(text,ifield{1});
    if isnumeric(a)
      dispc_no_line_end(str_,'c','b')
      dispc_no_line_end(['[',str.rm_nblank(num2str(size(a))),'] : '],'c')
      dispc(str.rm_nblank(num2str(a(1:min(numel(a),3)))),'c');
    elseif iscell(a)
      dispc_no_line_end(str_,'g','b')
      dispc(['cell   [',str.rm_nblank(num2str(size(a))),'] : '],'g');
    elseif islogical(a)
      dispc_no_line_end(str_,'w','b');
      if a==true 
        dispc('true','w');
      else
        dispc('false','w');
      end
    end
      %elseif isstruct(a)
      %dispc_no_line_end(str_,'m','b')
      %dispc(['struct   [',str.rm_nblank(num2str(size(a))),'] : '],'m');
      %end
  end
  for ifield=sort(fieldnames(text)')
    str_=[str.tab(ifield{1},ntab),' : '];
    a=getfield(text,ifield{1});
    if isstruct(a)
      dispc_no_line_end(str_,'y','b')
      dispc('struct','y');
    end
  end
elseif iscell(text)
  for icell=1:min(numel(text),30)
    dispc_no_line_end([num2str(icell),' : '],'r','b');
    if numel(text{icell}) < 40
      dispc(text{icell},'r');
    else
      dispc([text{icell}(1:40),'...',text{icell}(end-5:end)],'r')
    end
  end
end




    