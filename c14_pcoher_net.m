%class to show coherency at the scale of a network 
classdef c14_pcoher_net < handle 
       properties
        amp    =[];         
        coher  =[]; 
        isdata =[];
        lon    =[];
        lat    =[];
        dset   =[];
        ac_in = struct(); %metadata common to all station 
        in    = struct();
        date1 = [];
        date2 =[] ;
        day   =[] ;
        freq  =[] ;
    end
    methods
        function I_final = p_map_medmean(h,varargin) 
            in.cdate= datenum(2016,08,07)     ; 
            in.av_hr=24                       ;
            in.caxis=[0 0.012]                ;
            in.medmean_nhour =6               ;
            in.ss=100                         ;
            in.black=false                    ;
            in.colorbar_south=false           ;
            in=lang.parse_options(in,varargin);
            
            %I  = vecteur date utilise pour le calcul du smoothing
            %I2 = vecteur date sur lequel on calcul le medeman puis  dont on va prendre le max
            cdate2 = in.cdate+in.av_hr/24                       ;
            I = find(h.date2 >= in.cdate-in.medmean_nhour*2/24 & h.date2 <= cdate2+in.medmean_nhour*2/24);
            I2= find(h.date2(I)>=in.cdate & h.date2(I) <= cdate2);
            I_final = I(I2);
            medmean_date = h.date2(I_final);
            I_smooth=h.get_number_of_time_segment_corresponding_to_N_jour(in.medmean_nhour);
            nscale = size(h.coher,2);
            nsta  =  size(h.coher,3);
            ndate = numel(I)        ; 
            medmean=zeros(nscale,numel(I2),nsta);
            for iscale = 1 : nscale
                coher_  = squeeze(h.coher(I,iscale,:));
                medmean_ = movmedian(coher_,[I_smooth 0],'omitnan')- movmean(coher_,[I_smooth 0],'omitnan');
                medmean_ = medmean_(I2,:);
                medmean(iscale,:,:)=medmean_;
            end
            hold on;
            h.p_map_background('black',in.black);
            colormap(jet);
            m_scatter(h.lon,h.lat,in.ss,squeeze(max(max(medmean))),'fill');
            caxis(in.caxis);
            if in.colorbar_south 
                p.cb_south
            end


            %date1str= datestr(in.cdate,'yyyy-mm-dd-HH:MM:SS');
            %date2str= datestr(cdate2,'yyyy-mm-dd-HH:MM:SS');
            %titre=[date1str,'    ',date2str];
            %titre=[titre,'  max(medmean) smoothed over ',num2str(in.medmean_nhour),'h'];
            ylabel('median-mean','fontweight','bold','fontsize',14)
        end
        function I_date=p_map_coherency(h,varargin) 
            in.cdate= datenum(2016,08,07)     ; 
            in.av_hr=24                       ;
            in.caxis=[0.9 1]                  ;
            in.ss=100                         ;
            in.black=false                    ;
            in.colorbar_south=false           ;
            in.ylabel=true                    ;
            in=lang.parse_options(in,varargin);
            
            cdate2 = in.cdate+in.av_hr/24                       ;
            I_date=find(h.date2 >= in.cdate & h.date2 <= cdate2);
            coher = squeeze(mean(h.coher(I_date,:,:),'omitnan')) ;
            coher = min(coher);
        
            h.p_map_background('black',in.black);
            hold on;
            colormap(jet);
            m_scatter(h.lon,h.lat,in.ss,coher,'fill');
            caxis(in.caxis)
            %date1str= datestr(in.cdate,'yyyy-mm-dd-HH:MM:SS');
            %date2str= datestr(cdate2,'yyyy-mm-dd-HH:MM:SS');
            %titre=[date1str,'    ',date2str];
            %ylabel(titre,'fontweight','bold')
            if in.ylabel
                ylabel('coherency','fontweight','bold','fontsize',14);
            end
            if in.colorbar_south 
                p.cb_south;
            end
        end
        function p_map_background(h,varargin)
            in.black=false;
            in.dx=0;
            in.dy=0;
            in = lang.parse_options(in,varargin);
            
            inm.topo=false;
            if in.black 
                inm.coastcol=[1 1 1]; 
                inm.mgrid_color=[1 1 1]*0.5;
            else
                inm.coastcol=[0 0 0]; 
            end
            lon=[min(h.lon)-in.dx max(h.lon)+in.dx];
            lat=[min(h.lat)-in.dy max(h.lat)+in.dy];
            map.background(lon,lat,inm);
            
            %I=find(strcmp(h.dset,'/IV/NRCA.00')==1);
            %map.station(h.lon(I),h.lat(I),'mark','^','msize',20)
        end
    end
    methods %utilitarian methods. 
        function I_smooth=get_number_of_time_segment_corresponding_to_N_jour(h,in_nhour)
            step_second = (h.date2(2)-h.date2(1))*86400;
            I_smooth = floor(in_nhour*3600 / step_second);
        end
    end
    methods 
        function h=c14_pcoher_net(in_dir)
        % here we concatenate the coherency measurements contain in each .mat file located in in_dir
        % we assume that they were all done with the same parameters, and that all timeseries 
        % correspond to the same date. 
            file_list = os.dir([in_dir,'/*/*mat']);
            nsta     = numel(file_list);
            for ista = 1:nsta
                s=load(file_list{ista});
                if ista ==1 
                    [nseg ndate]= size(s.amp)     ; 
                    nscale      = size(s.coher,1) ;
                    h.amp   = nan(nsta,nseg*ndate);
                    h.coher = nan(nseg*ndate,nscale,nsta);
                    h.isdata=nan(ndate,nsta)      ;
                    h.lon    =nan(1,nsta)         ;
                    h.lat    =nan(1,nsta)         ;
                    h.dset   = cell(1,nsta)       ;
                end
                h.amp(ista,:) = s.amp(:)       ;
                h.coher(:,:,ista)=s.coher(:,:)';
                h.isdata(:,ista) = s.isdata    ;
                h.lon(ista) = s.sta.lon        ; 
                h.lat(ista) = s.sta.lat        ;
                h.dset(ista) = {s.sta.dset}    ;
            end
            h.ac_in = s.ac_in      ; 
            h.date1 = s.date1(:)   ; 
            h.date2 = s.date2(:)   ; 
            h.day   = s.day        ; 
            h.freq  = s.freq       ; 
            h.in    = s.in         ; 
        end
    end
end
