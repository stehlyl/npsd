function sta_lta = compute(data,tau,win,p1,p2)
% data  : data over which we compute the sta_lta
% tau   : sampling rate of the data [s]
% win   : structure containing the indice of the sta and lta window. 
%       : retrived from sta_lta.get_window();
% p1,p2 : array of period band over which the sta/lta is computed 
%
%----------
% OUTPUT
% sta_lta : 2D matrix [nper,nwin] that contain the ratio of the max_ of each STA window over the standard
%         : deviation of each LTA window.

    %
    nper = numel(p1); 
    nwin = numel(win.I_sta1);

    %prepare the output : 
    sta_lta = struct(); 
    sta_lta.p1 = p1   ; 
    sta_lta.p2 = p2   ; 
    sta_lta.win= win  ; 
    sta_lta.sta_lta = zeros(nper,nwin);

    % loop on each period band and window :  
    for iper =1 : nper
        data_stalta  = s2d.filter(data,p1(iper),p2(iper),tau); 
        for iwin = 1 : nwin 
            I1 = win.I_sta1(iwin);
            I2 = win.I_sta2(iwin); 
            max_ = max(abs(data_stalta(I1:I2)));
            std_ = std( data_stalta([win.I_lta1(iwin) : win.I_lta2(iwin)]));
            sta_lta.sta_lta(iper,iwin) = max_ / std_; 
        end
    end
end