function win = get_window(nwf,tau,sta_length, lta_length)
    sta_pts = ceil(sta_length /tau) ; 
    lta_pts = ceil(lta_length /tau) ;
    
    % designe the STA win : 
    I_sta1 = [1 : ceil(sta_pts/2) : nwf]; 
    I_sta2 = I_sta1 + sta_pts ; 
    I = find(I_sta2 <= nwf);
    I_sta1 = I_sta1(I); 
    I_sta2 = I_sta2(I); 
    I_sta2(end+1)=nwf; 
    I_sta1(end+1)=nwf-sta_pts;
    % now get the LTA win 
    nwin = numel(I_sta1); 
    I_lta1 = zeros(1,nwin); 
    I_lta2 = zeros(1,nwin);
    for iwin = 1 : nwin 
        I_lta1(iwin) = max(I_sta1(iwin) - lta_pts/2 ,1);
        I_lta2(iwin) = min(nwf, I_lta1(iwin) + lta_pts*2); 
        if I_lta2(iwin) >= nwf ;
            I_lta1(iwin) = I_lta2(iwin)- lta_pts;
        end
    end
    win = struct()     ;
    win.I_lta1 = I_lta1; 
    win.I_lta2 = I_lta2; 
    win.I_sta1 = I_sta1;
    win.I_sta2 = I_sta2;

